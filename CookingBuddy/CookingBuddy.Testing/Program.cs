﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CookingBuddy.Utility;
using CookingBuddy.Data;
using CookingBuddy.DataManager;

namespace CookingBuddy.Testing
{
    class Program
    {
        static void Main(string[] args)
        {
            var jsonStr = "{\"recipeName\" : \"Devil's Food Cake\",\"author\" : \"Alton Brown\",\"prep\" : 20,\"inactive\" : 90,\"cooking\" : 35,\"instructions\" : [\"Set a rack in the middle of the oven and preheat to 325 degrees F. Spray a 13 by 9-inch metal pan with nonstick spray, line with parchment paper so it hangs over the sides of the pan and spray the parchment with nonstick spray. Set aside.\", \"Whisk the boiling water and cocoa powder together in a small bowl and set aside.\", \"Combine the sugar, flours, baking soda and salt in the bowl of a stand mixer fitted with the paddle attachment. Whisk the oil, sour cream, eggs and egg yolks in a large pourable vessel. Add the oil mixture to the cocoa and water mixture and slowly whisk to combine. With the mixer on low speed, add the liquid mixture to the dry mixture over 30 seconds. Continue to beat on low speed for another 30 seconds. Stop and scrape down the sides of the bowl. Continue to beat on low speed until the batter is smooth, 10 to 15 seconds. Pour the batter into the prepared pan and bake until the cake springs back when pressed and reaches an internal temperature of 205 degrees F, 30 to 35 minutes.\",\"Cool in the pan on a rack for 30 minutes, and then remove cake from the pan and cool completely before frosting, about 1 hour.\"],\"ingredients\" : [{\"foodId\" : null, \"foodName\" : \"Nonstick spray\", \"quantity\" : 1, \"measurementType\" : 1},{\"foodId\" : null, \"foodName\" : \"All-purpose flour\", \"quantity\" : 5.5, \"measurementType\" : \"ounces\"}],\"userId\" : \"38CA497D-5CE5-4372-BCDC-5ED08952B1D3\"}";
            var json = JSON.Parse(jsonStr);

            var manager = new RecipeManager();

            var recipe = manager.CreateRecipe(json.AsObject);

            Console.WriteLine("Recipe Name: " + recipe.RecipeName);
            Console.WriteLine("Recipe Author: " + recipe.Author);
            Console.WriteLine("Recipe Id: " + recipe.RecipeId);
            Console.WriteLine("");

            var sb = new StringBuilder();
            var ingredients = recipe.Ingredients;
            foreach(var i in ingredients)
            {
                sb.AppendLine(string.Format("{0} {1} {2}", i.Quantity, i.MeasurementUnitId, i.Food.FoodName));
            }

            Console.WriteLine("Ingredients: " + ingredients.Count);
            Console.WriteLine(sb.ToString());

            Console.ReadLine();
        }
    }
}
