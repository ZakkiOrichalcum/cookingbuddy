﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PetaPoco;

namespace CookingBuddy.Data
{
    [TableName("Foods")]
    [PrimaryKey("FoodId")]
    [Serializable]
    public class Food : PocoBase
    {
        private const string GET_BY_ID =
@"SELECT * FROM Foods
  WHERE FoodId = @0";
        private const string GET_BY_NAME =
@"SELECT * FROM Foods
  WHERE FoodName = @0";

        public Guid FoodId { get; set; }
        public string FoodName { get; set; }
        public string Plurals { get; set; }
        public int FoodTypeId { get; set; }
        public double DensityConverstion { get; set; }

        internal new virtual void Insert() { base.Insert(); }

        internal static Food GetById(Guid id)
        {
            using (var database = new Database())
            {
                return database.Query<Food>(GET_BY_ID, id).FirstOrDefault();
            }
        }

        internal static Food GetByName(string name)
        {
            using (var database = new Database())
            {
                return database.Query<Food>(GET_BY_NAME, name).FirstOrDefault();
            }
        }
    }
}
