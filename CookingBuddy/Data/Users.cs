﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PetaPoco;

namespace CookingBuddy.Data
{
    [TableName("Users")]
    [PrimaryKey("UserId")]
    [Serializable]
    public class User : PocoBase
    {
        private const string GET_BY_ID =
@"SELECT * FROM Users
  WHERE UserId = @0";

        public Guid UserId { get; set; }
        public string UserName { get; set; }

        internal new virtual void Insert() { base.Insert(); }

        internal static Recipe GetById(Guid id)
        {
            using (var database = new Database())
            {
                return database.Query<Recipe>(GET_BY_ID, id).FirstOrDefault();
            }
        }
    }
}
