﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PetaPoco;

namespace CookingBuddy.Data
{
    [TableName("RecipeTags")]
    [PrimaryKey("TagId")]
    [Serializable]
    public class RecipeTag : PocoBase
    {
        private const string GET_BY_ID =
@"SELECT * FROM RecipeTags
  WHERE RecipeId = @0";

        public int TagId { get; set; }
        public Guid RecipeId { get; set; }
        public string Tags { get; set; }

        internal static IEnumerable<RecipeTag> GetByRecipeId(Guid id)
        {
            using (var database = new Database())
            {
                return database.Query<RecipeTag>(GET_BY_ID, id);
            }
        }
    }
}
