﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PetaPoco;

namespace CookingBuddy.Data
{
    [TableName("ScheduledRecipes")]
    [PrimaryKey("ScheduleId")]
    [Serializable]
    public class ScheduledRecipe : PocoBase
    {
        private const string GET_BY_ID =
@"SELECT * FROM ScheduledRecipes
  WHERE ScheduleId = @0";

        private const string GET_BY_RECIPE_ID =
@"SELECT * FROM ScheduledRecipes
  WHERE RecipeId = @0";

        public Guid ScheduleId { get; set; }
        public Guid RecipeId { get; set; }
        public Guid UserId {get;set;}
        public DateTime ScheduledDateTime { get; set; }

        public static IEnumerable<ScheduledRecipe> GetByRecipeId(string id)
        {
            using (var database = new Database())
            {
                return database.Query<ScheduledRecipe>(GET_BY_ID, id);
            }
        }
    }
}
