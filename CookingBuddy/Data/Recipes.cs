﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PetaPoco;

namespace CookingBuddy.Data
{
    [TableName("Recipes")]
    [PrimaryKey("RecipeId")]
    [Serializable]
    public class Recipe : PocoBase
    {
        private const string GET_BY_ID =
@"SELECT * FROM Recipes
  WHERE RecipeId = @0 AND IsDeleted = 0";
        private const string GET_ALL =
@"SELECT * FROM Recipes
  WHERE IsDeleted = 0 AND IsDraft = 0";

        public Guid RecipeId { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsDraft { get; set; }
        public string RecipeName { get; set; }
        public string Author { get; set; }
        public Guid UserId { get; set; }
        public double? PreparationTime { get; set; }
        public double? InactiveTime { get; set; }
        public double? CookingTime { get; set; }
        public string RecipeInstructionJSON { get; set; }
        public string OriginalSource { get; set; }

        [ResultColumn]
        public List<Ingredient> Ingredients
        {
            get
            {
                return Ingredient.GetByRecipeId(RecipeId).ToList();
            }
        }

        internal new virtual void Insert() { base.Insert(); }

        internal static Recipe GetById(string id)
        {
            using (var database = new Database())
            {
                return database.Query<Recipe>(GET_BY_ID, id).FirstOrDefault();
            }
        }

        internal static IEnumerable<Recipe> GetAll()
        {
            using (var database = new Database())
            {
                return database.Query<Recipe>(GET_ALL);
            }
        }
    }
}
