﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PetaPoco;

namespace CookingBuddy.Data
{
    [TableName("PantryItems")]
    [PrimaryKey("PantryId")]
    [Serializable]
    public class PantryItem : PocoBase
    {
        private const string GET_BY_ID =
@"SELECT * FROM PantryItems
  WHERE PantryId = @0";

        private const string GET_BY_USER_ID =
@"SELECT * FROM PantryItems
  WHERE UserId = @0";

        public Guid PantryId { get; set; }
        public Guid UserId { get; set; }
        public Guid FoodId { get; set; }
        public double Quantity { get; set; }
        public int MeasurementUnitId { get; set; }
        public DateTime ScheduledDateTime { get; set; }

        public static IEnumerable<PantryItem> GetByUserId(Guid id)
        {
            using (var database = new Database())
            {
                return database.Query<PantryItem>(GET_BY_USER_ID, id);
            }
        }
    }
}
