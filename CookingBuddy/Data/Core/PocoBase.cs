﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using PetaPoco;

namespace CookingBuddy.Data
{
    [Serializable]
    public class PocoBase
    {
        #region DEPTH MANAGEMENT
        internal const int MAX_DEPTH_FOR_LAZY_LOADING = 2;

        [JsonIgnore]
        internal int _objectDepth = 1;
        [JsonIgnore]
        internal bool _overrideLazyLoadDepthRestrictions;

        internal bool _isLazyLoadingAllowed()
        {
            return _objectDepth <= MAX_DEPTH_FOR_LAZY_LOADING;
        }
        internal void _setDepthManagementProperties(int parentDepth, bool parentOverrideLazyLoadDepth)
        {
            _objectDepth = parentDepth + 1;
            _overrideLazyLoadDepthRestrictions = parentOverrideLazyLoadDepth;
        }
        #endregion


        internal virtual object Insert()
        {
            return Insert(this);
        }
        private static object Insert<T>(T poco)
        {
            using (var pocoDC = new Database())
            {
                return pocoDC.Insert(poco);
            }
        }

        internal virtual void Update()
        {
            Update(this);
        }
        private static void Update<T>(T poco)
        {
            using (var pocoDC = new PetaPoco.Database())
            {
                pocoDC.Update(poco);
            }
        }

        internal virtual void Delete()
        {
            Delete(this);
        }
        private static void Delete<T>(T poco)
        {
            using (var pocoDC = new PetaPoco.Database())
            {
                pocoDC.Delete(poco);
            }
        }

        public static string GetJavascriptFriendlyDateTime(DateTime dateTimeIn)
        {
            return dateTimeIn.ToString("ddd MMM d yyyy HH:mm:ss");
        }

        internal static T FromJsonTyped<T>(string jsonString)
        {
            return JsonConvert.DeserializeObject<T>(jsonString, new JsonSerializerSettings()
            {

            });
        }

        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.None, new JsonSerializerSettings()
            {
                ContractResolver = new NonDeepNonDetailedContractResolver()

            });
        }
        public string ToDetailedJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.None, new JsonSerializerSettings()
            {
                ContractResolver = new NonDeepContractResolver()

            });
        }
        public string ToDeepJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.None, new JsonSerializerSettings()
            {
                ContractResolver = new NonDetailedContractResolver()
            });
        }
        public string ToDeepDetailedJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.None, new JsonSerializerSettings()
            {

            });
        }
    }

    public interface IDepthManageable
    {
        bool _isLazyLoadingAllowed();
        void _setDepthManagementProperties(int parentDepth, bool parentOverrideLazyLoadDepth);
    }
    public interface IHasRequiredData
    {
        bool HasRequiredData();
    }
}
