﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using PetaPoco;

namespace CookingBuddy.Data
{
    public class CustomPropertyContractResolverBase : DefaultContractResolver
    {
        protected bool ShouldSerializeDetailed { get; set; }
        protected bool ShouldSerializeDeep { get; set; }

        public CustomPropertyContractResolverBase(bool shouldSerializeDetailed, bool shouldSerializeDeep)
        {
            ShouldSerializeDetailed = shouldSerializeDetailed;
            ShouldSerializeDeep = shouldSerializeDeep;
        }

        protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization)
        {
            var property = base.CreateProperty(member, memberSerialization);
            var shouldSerialize = (ShouldSerializeDeep || (!ShouldSerializeDeep && Attribute.GetCustomAttribute(member, typeof(JsonDeepObject)) == null))
                               && (ShouldSerializeDetailed || (!ShouldSerializeDetailed && Attribute.GetCustomAttribute(member, typeof(JsonDetail)) == null));
            if (!shouldSerialize)
            {
                property.ShouldSerialize = instance => false;
            }

            return property;
        }

    }

    internal class NonDeepNonDetailedContractResolver : CustomPropertyContractResolverBase
    {
        public NonDeepNonDetailedContractResolver() : base(false, false) { }
    }
    internal class NonDeepContractResolver : CustomPropertyContractResolverBase
    {
        public NonDeepContractResolver() : base(true, false) { }
    }
    internal class NonDetailedContractResolver : CustomPropertyContractResolverBase
    {
        public NonDetailedContractResolver() : base(false, true) { }
    }

    [AttributeUsage(AttributeTargets.All)]
    public class JsonDeepObject : ColumnAttribute
    {

    }
    [AttributeUsage(AttributeTargets.All)]
    public class JsonDetail : ColumnAttribute
    {

    }
}
