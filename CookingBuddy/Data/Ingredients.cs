﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PetaPoco;

namespace CookingBuddy.Data
{
    [TableName("RecipeIngredients")]
    [PrimaryKey("IngredientsId")]
    [Serializable]
    public class Ingredient : PocoBase
    {
        private const string GET_BY_RECIPE_ID =
@"SELECT * FROM RecipeIngredients
  WHERE RecipeId = @0";

        public Guid IngredientsId { get; set; }
	    public Guid RecipeId { get; set; }
	    public Guid FoodId { get; set; }
	    public double Quantity { get; set; }
	    public int MeasurementUnitId { get; set; }

        [ResultColumn]
        public Food Food
        {
            get
            {
                return Food.GetById(FoodId);
            }
        }

        internal new virtual void Insert() { base.Insert(); }

        internal static IEnumerable<Ingredient> GetByRecipeId(Guid id)
        {
            using (var database = new Database())
            {
                return database.Query<Ingredient>(GET_BY_RECIPE_ID, id);
            }
        }
    }
}
