﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace CookingBuddy.DataManager.DataObjects
{
    public interface IResponse
    {
        Object Payload { get; set; }
        string Message { get; set; }
        HttpStatusCode Status { get; set; }
    }
}
