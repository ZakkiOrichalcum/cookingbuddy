﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CookingBuddy.Utility;
using CookingBuddy.Data;

namespace CookingBuddy.DataManager
{
    public class RecipeManager
    {
        public Recipe GetRecipeFromId(string id)
        {
            return Recipe.GetById(id);
        }

        public IEnumerable<Recipe> GetAllRecipe()
        {
            return Recipe.GetAll();
        }

        public Recipe CreateRecipe(string name, string author, double? prep, double? inactive, double? cooking, string recipeJSON, string originalSource, JSONArray ingredientsJson, Guid userId)
        {
            var recipe = new Recipe()
            {
                RecipeId = Guid.NewGuid(),
                IsDeleted = false,
                IsDraft = false,
                RecipeName = name,
                Author = author,
                UserId = userId,
                PreparationTime = prep,
                InactiveTime = inactive,
                CookingTime = cooking,
                RecipeInstructionJSON = recipeJSON,
                OriginalSource = originalSource
            };

            recipe.Insert();

            for (var i = 0; i < ingredientsJson.Count; i++)
            {
                var iJson = ingredientsJson[i];

                var food = (!string.IsNullOrEmpty(iJson["foodId"])) ? Food.GetById(Guid.Parse(iJson["foodId"].QuotelessValue)) :
                    (!string.IsNullOrEmpty(iJson["foodName"])) ? Food.GetByName(iJson["foodName"].QuotelessValue) : null;

                if(food == null)
                {
                    var foodManager = new FoodManager();
                    food = foodManager.CreateFoodMinimally(iJson["foodName"].QuotelessValue);
                }

                var ingredient = new Ingredient()
                {
                    FoodId = food.FoodId,
                    Quantity = iJson["quantity"].AsDouble,
                    MeasurementUnitId = 0,
                    RecipeId = recipe.RecipeId
                };

                ingredient.Insert();
            }

            return recipe;
        }

        public Recipe CreateRecipe(JSONClass json)
        {
            return CreateRecipe(json["recipeName"].QuotelessValue, json["author"].QuotelessValue, json["prep"].AsDouble,
                                json["inactive"].AsDouble, json["cooking"].AsDouble, json["instructions"].ToString(), 
                                json["originalSource"].QuotelessValue, json["ingredients"].AsArray, Guid.Parse(json["userId"].QuotelessValue));
        }
    }
}
