﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CookingBuddy.Utility;
using CookingBuddy.Data;

namespace CookingBuddy.DataManager
{
    public class FoodManager
    {
        public Food CreateFoodMinimally(string foodName)
        {
            var food = new Food()
            {
                FoodId = Guid.NewGuid(),
                FoodName = foodName
            };

            food.Insert();

            return food;
        }
    }
}
