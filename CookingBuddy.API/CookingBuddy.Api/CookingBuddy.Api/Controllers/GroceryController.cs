﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CookingBuddy.Data;
using CookingBuddy.DataManager;
using CookingBuddy.DataManager.DataObjects;

namespace CookingBuddy.Api.Controllers
{
    public class GroceryController : Controller
    {
        public IResponse GetGroceryList(Guid accountId)
        {
            throw new NotImplementedException();
        }

        public IResponse AddToGroceryList(Guid accountId, Guid itemId, int quantity, DateTime timeNeeded = new DateTime(), bool share = false)
        {
            throw new NotImplementedException();
        }

        public IResponse RemoveFromGroceryList(Guid accountId, Guid itemId, int quantity)
        {
            throw new NotImplementedException();
        }

        public IResponse GetGroceryListBeforeDateTime(Guid accountId, DateTime cutOffTime)
        {
            throw new NotImplementedException();
        }
    }
}