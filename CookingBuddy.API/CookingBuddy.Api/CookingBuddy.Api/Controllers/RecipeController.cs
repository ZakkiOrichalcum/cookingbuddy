﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CookingBuddy.DataManager.DataObjects;

namespace CookingBuddy.Api.Controllers
{
    public class RecipeController : Controller
    {
        public IResponse GetListOfRecipes(ISearchParameters searchParameters)
        {
            throw new NotImplementedException();
        }

        public IResponse GetRecipe(Guid recipeId)
        {
            throw new NotImplementedException();
        }

        public IResponse AddRecipeToSchedule(Guid accountId, Guid recipeId, DateTime scheduledDate)
        {
            throw new NotImplementedException();
        }

        public IResponse RemoveRecipeFromSchedule(Guid accountId, Guid scheduledRecipeId)
        {
            throw new NotImplementedException();
        }

        public IResponse AddRatingToRecipe(Guid recipeId, double rating)
        {
            throw new NotImplementedException();
        }

        public IResponse AddRecipeToFavorites(Guid accountId, Guid recipeId)
        {
            throw new NotImplementedException();
        }

        public IResponse RemoveRecipeFromFavorites(Guid accountId, Guid recipeId)
        {
            throw new NotImplementedException();
        }
    }
}