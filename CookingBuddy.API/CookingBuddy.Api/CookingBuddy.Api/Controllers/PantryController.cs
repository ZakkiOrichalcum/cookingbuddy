﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CookingBuddy.DataManager.DataObjects;

namespace CookingBuddy.Api.Controllers
{
    public class PantryController : Controller
    {
        public IResponse GetPantryItems(Guid accountId, bool useSharedItems = false)
        {
            throw new NotImplementedException();
        }

        public IResponse AddPantryItem(Guid accountId, Guid itemId, int quantity)
        {
            throw new NotImplementedException();
        }

        public IResponse RemovePantryItem(Guid accountId, Guid itemId, int quantity)
        {
            throw new NotImplementedException();
        }
    }
}