﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using CookingBuddy.Utility;

public class RecipeListController : MonoBehaviour {

    public Image Image;
    public Text Title;
    public Text Author;
    public Text Prep;
    public Text Inactive;
    public Text Cook;
    public Text Description;
    public Text Tags;

    public void Initialize(JSONNode json)
    {

    }
}
